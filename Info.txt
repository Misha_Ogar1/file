The result of a pilot clinical study was an improvement in the vision of all participants. Although 14 out of 20 volunteers were blind before the operation, after two years they were all able to see.

However, before the implant can be widely used, scientists need to conduct a larger clinical trial, as well as gain approval from national regulatory authorities.


Об этом сообщает ТАСС.

Горбачёва похоронили в родовом захоронении рядом с супругой Раисой. Похороны прошли при участии роты почётного караула.

Ранее в Колонном зале Дома союзов в Москве завершилась церемония прощания с Горбачёвым.

Горбачёв скончался в возрасте 91 года 30 августа после тяжёлой и продолжительной болезни.