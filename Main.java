import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {
//        //Запись в файл
//        try {
//            PrintWriter writer = new PrintWriter("out/file.txt");
//            for (int i = 0; i < 1000; i++) {
//                writer.write(i + "\n");
//            }
//            writer.flush();
//            writer.close();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

//        Чтение файла
        StringBuilder builder = new StringBuilder();

        try {
            List<String> lines = Files.readAllLines(Paths.get("C:\\file\\info.txt"));
            lines.forEach(line -> builder.append(lines + "\n"));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        System.out.println(builder.toString());

        //BufferedReader
        try{
            BufferedReader br = new BufferedReader(new FileReader("C:\\file\\info.txt"));

            for(;;) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }

                builder.append(line + "\n\r");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            FileInputStream is = new FileInputStream("C:\\file\\info.txt");

            for (;;) {
                int code = is.read();
                if (code < 0) {
                    break;
                }

                char ch = (char) code;
                builder.append(ch);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //Windows-1251

        String text = String.valueOf(builder);

        try {
            String encodedText = new String(text.getBytes(), "UTF-8");
            System.out.println(encodedText.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}
